


// import 'blueimp-file-upload';
import {DOM} from './_const';

import './modules/main-banner'

const onContentLoaded = () => {

    let lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));
  
    const lazyLoad = () => {
      lazyImages.forEach((lazyImage) => {
        const topBreakpoint = lazyImage.classList.contains('lazy_last') ?
                              window.innerHeight + 500 : window.innerHeight + 200;
  
        if ((lazyImage.getBoundingClientRect().top <= topBreakpoint
            && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== 'none') {
          // eslint-disable-next-line
          lazyImage.src = lazyImage.dataset.src;
          // eslint-disable-next-line
          lazyImage.srcset = lazyImage.dataset.srcset;
          lazyImage.classList.remove('lazy');
  
          lazyImages = lazyImages.filter((image) => {
          return image !== lazyImage;
          });
  
          if (lazyImages.length === 0) {
            DOM.document.removeEventListener('scroll', lazyLoad);
            DOM.window.removeEventListener('resize', lazyLoad);
            DOM.window.removeEventListener('orientationchange', lazyLoad);
          }
        }
      });
    };
  
    DOM.doc.addEventListener('scroll', lazyLoad);
    DOM.win.addEventListener('resize', lazyLoad);
    DOM.win.addEventListener('orientationchange', lazyLoad);
  }











// const upload = () => {

//   $('#fileupload').fileupload({
//     done (e, data) {
//       $.each(data.result.files, function uploadF(index, file) {
//             $('<p/>').text(file.name).appendTo(document.body);
//         });
//     }
//   });

// }




  

const onLoad = () => {
}
  
  
  
$(DOM.doc).ready(onLoad);
DOM.doc.addEventListener('DOMContentLoaded', onContentLoaded);
  

$( document ).ready(function() {
  $( ".seo-wrap .read-more" ).click(function(e) {
    e.preventDefault();
    $(this).parents('.seo-wrap').toggleClass('open');
    const openPar = $(this).parents('.seo-wrap').hasClass('open');
    if (openPar){
      $(this).find('span').text('Скрыть');
    } else {
      $(this).find('span').text('Читать полностью');
    }
  });
});