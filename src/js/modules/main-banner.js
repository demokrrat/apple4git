/* eslint-disable no-unused-vars */
/* eslint-disable func-names */
import Swiper from 'swiper';

$('.js-single-banner').each(function (index, element) {
  // element == this

  const arrowPrev = $(this).find('.btn-slider-nav-prev')
  const arrowNext = $(this).find('.btn-slider-nav-next')

  const bannerSlider = new Swiper ($(this), {
    // init: false,
    loop: true,
    slidesPerView: 1,
    pagination: {
      el: '.pagina-slider-main',
      type: 'bullets',
      clickable: true
    },
    navigation: {
      
      nextEl: arrowNext,
      prevEl: arrowPrev,
    },
  });
  
});


$('.js-popular-products').each(function (index, element) {
  // element == this

  const arrowPrev = $(this).find('.slider-popular-tabs-nav-prev')
  const arrowNext = $(this).find('.slider-popular-tabs-nav-next')

  const bannerSlider = new Swiper ($(this), {
    // init: false,
    loop: true,
    slidesPerView: 'auto',
    navigation: {
      
      nextEl: arrowNext,
      prevEl: arrowPrev,
    },
    
  });
  
});

$('.js-our-everyday-slider').each(function (index, element) {
  // element == this

  const arrowPrev = $(this).find('.our-everyday-nav-prev')
  const arrowNext = $(this).find('.our-everyday-nav-next')

  const bannerSlider = new Swiper ($(this), {
    // init: false,
    slidesPerView: 'auto',
    navigation: {
      
      nextEl: arrowNext,
      prevEl: arrowPrev,
    },
  });
  
});